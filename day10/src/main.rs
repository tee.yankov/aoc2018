use std::io::{Read,Error};
use std::fs::File;
use std::str::FromStr;
use std::cmp::{max,min};

#[derive(Debug)]
struct Point {
    position: (i32, i32),
    velocity: (i32, i32),
}

impl FromStr for Point {
    type Err = Error;

    fn from_str(s: &str) -> Result<Point, Error> {
        let mut buf: String = String::new();
        let mut coords: Vec<i32> = vec![];
        for c in s.chars() {
            match c {
                '-' | '0'..='9' => {
                    buf.push(c);
                },
                _ => {
                    if !buf.is_empty() {
                        coords.push(buf.parse().unwrap());
                    }
                    buf = String::new();
                }
            }
        };
        Ok(Point {
            position: (coords[0], coords[1]),
            velocity: (coords[2], coords[3]),
        })
    }
}

impl Point {
    fn next_position(&mut self) {
        self.position.0 += self.velocity.0;
        self.position.1 += self.velocity.1;
    }

    fn intersects(&self, x: i32, y: i32) -> bool {
        self.position.0 == x && self.position.1 == y
    }
}

#[derive(Debug)]
struct Skybox {
    points: Vec<Point>,
    bounding_box: (i32, i32, i32, i32),
}

impl Skybox {
    fn new(points: Vec<Point>) -> Skybox {
        let bounding_box = points.iter().fold((std::i32::MAX, std::i32::MIN, std::i32::MAX, std::i32::MIN), |mut acc, point| {
            acc.0 = min(acc.0, point.position.0);
            acc.1 = max(acc.1, point.position.0);
            acc.2 = min(acc.2, point.position.1);
            acc.3 = max(acc.3, point.position.1);
            acc
        });

        Skybox {
            points,
            bounding_box,
        }
    }

    fn draw(&self) {
        for y in self.bounding_box.2..=self.bounding_box.3 {
            for x in self.bounding_box.0..=self.bounding_box.1 {
                if self.points.iter().all(|p| !p.intersects(x, y)) {
                    print!(".");
                } else {
                    print!("#");
                }
            }
            print!("\n");
        }
    }

    fn tick(&mut self) {
        let mut min_x = std::i32::MAX;
        let mut max_width = std::i32::MIN;
        let mut min_y = std::i32::MAX;
        let mut max_height = std::i32::MIN;
        for point in &mut self.points {
            point.next_position();

            min_x = min(min_x, point.position.0);
            max_width = max(max_width, point.position.0);
            min_y = min(min_y, point.position.1);
            max_height = max(max_height, point.position.1);
        }

        self.bounding_box = (min_x, max_width, min_y, max_height);
    }
}

fn main() -> Result<(), Error> {
    let mut input_file = File::open("./input")?;
    let mut input = String::new();
    input_file.read_to_string(&mut input)?;
    let points: Vec<Point> = input.trim().lines().map(|x| x.parse().unwrap()).collect();
    let mut skybox = Skybox::new(points);
    println!("{} {}", skybox.bounding_box.1 - skybox.bounding_box.0, skybox.bounding_box.3 - skybox.bounding_box.2);
    for i in 0..100_000_000 {
        let skybox_width = skybox.bounding_box.1 - skybox.bounding_box.0;
        let skybox_height = skybox.bounding_box.3  - skybox.bounding_box.2;
        print!("{}x{}\r", skybox_width, skybox_height);
        if skybox_width <= 80 && skybox_height <= 80 {
            println!("{} second", i);
            skybox.draw();
            print!("\n");
        }
        skybox.tick();
    }
    Ok(())
}
