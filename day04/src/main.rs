use chrono::prelude::*;
use std::collections::{BTreeMap, HashMap};
use std::fs::File;
use std::io::Read;

#[derive(Debug)]
enum TimeSlotKind {
    WakeUp,
    FallAsleep,
}

#[derive(Debug)]
struct TimeSlot {
    kind: TimeSlotKind,
    ts: DateTime<Utc>,
}

#[derive(Debug)]
struct Guards {
    guards: HashMap<i32, Vec<TimeSlot>>,
}

impl Guards {
    fn new() -> Guards {
        Guards {
            guards: HashMap::new(),
        }
    }

    fn insert(&mut self, guard_id: i32, ts: TimeSlot) {
        if self.guards.contains_key(&guard_id) {
            self.guards.get_mut(&guard_id).unwrap().push(ts);
        } else {
            self.guards.insert(guard_id, vec![ts]);
        }
    }
}

fn main() {
    let mut input_file = File::open("./input").expect("Could not open input file");
    let mut input = String::new();
    input_file
        .read_to_string(&mut input)
        .expect("Could not read input file");
    let guards = parse_input(&input);
    let time_asleep: Vec<(i32, (u32, u16, u16))> = guards.guards.iter().map(|(&k, v)| (k, calculate_time_asleep(v))).collect();
    let sleepiest_guard = time_asleep.iter().fold(None, |acc: Option<(i32, (u32, u16, u16))>, v| {
        if let Some(acc) = acc {
            if (acc.1).0 > (v.1).0 {
                Some(acc)
            } else {
                Some(*v)
            }
        } else {
            Some(*v)
        }
    }).unwrap();
    let sleepiest_guard2 = time_asleep.iter().fold(None, |acc: Option<(i32, (u32, u16, u16))>, v| {
        if let Some(acc) = acc {
            if (acc.1).2 > (v.1).2 {
                Some(acc)
            } else {
                Some(*v)
            }
        } else {
            Some(*v)
        }
    }).unwrap();
    println!("Sleepiest Guard: #{}, who is sleepiest {} minutes after midnight", sleepiest_guard.0, (sleepiest_guard.1).1);
    println!("Solution: {}", sleepiest_guard.0 * (sleepiest_guard.1).1 as i32);
    println!("Sleepiest Guard by Minute: #{}, who is sleepiest {} minutes after midnight", sleepiest_guard2.0, (sleepiest_guard2.1).1);
    println!("Solution #2: {}", sleepiest_guard2.0 * (sleepiest_guard2.1).1 as i32);
}

fn parse_input(input: &String) -> Guards {
    let time_entries: BTreeMap<DateTime<Utc>, String> = input
        .lines()
        .map(|line| {
            let mut bytes = line.bytes();
            bytes.next(); // Skip the first char
            let timestring: String = String::from_utf8(
                bytes
                    .by_ref()
                    .take_while(|x| *x != ']' as u8)
                    .collect::<Vec<u8>>(),
            )
            .unwrap();
            let ts = Utc
                .datetime_from_str(&timestring, "%Y-%m-%d %H:%M")
                .expect("Could not create date from string");
            let rest = String::from_utf8(bytes.collect()).unwrap();
            (ts, rest.trim().into())
        })
        .collect();

    let mut current_guard_id: Option<i32> = None;
    let mut guards = Guards::new();
    for (k, v) in time_entries {
        let parts: Vec<&str> = v.split(' ').collect();
        match parts[0] {
            "Guard" => {
                let guard_id: i32 = parts[1][1..].parse().unwrap();
                current_guard_id = Some(guard_id);
            }
            "wakes" => {
                guards.insert(
                    current_guard_id.unwrap(),
                    TimeSlot {
                        kind: TimeSlotKind::WakeUp,
                        ts: k,
                    },
                );
            }
            "falls" => guards.insert(
                current_guard_id.unwrap(),
                TimeSlot {
                    kind: TimeSlotKind::FallAsleep,
                    ts: k,
                },
            ),
            &_ => {}
        }
    }

    guards
}

fn calculate_time_asleep (time_slots: &Vec<TimeSlot>) -> (u32, u16, u16) {
    let mut time_slots_iter = time_slots.iter();
    let mut most_important_minute: [u16; 60] = [0; 60];
    let mut total_time_asleep = 0;
    while let (Some(asleep), Some(awake)) = (time_slots_iter.next(), time_slots_iter.next()) {
        for min in asleep.ts.minute()..awake.ts.minute() {
            most_important_minute[min as usize] += 1;
        }
        total_time_asleep += awake.ts.minute() - asleep.ts.minute();
    };
    let mut largest_minute = 0;
    let mut largest_minute_index = 0;
    for (i, v) in most_important_minute.iter().enumerate() {
        if *v > largest_minute {
            largest_minute = *v;
            largest_minute_index = i;
        }
    }
    (total_time_asleep, largest_minute_index as u16, largest_minute)
}
