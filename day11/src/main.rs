use rayon::prelude::*;
use fnv::FnvHashMap;

const SERIAL_NUM: i32 = 7400;

struct Cell {
    power_level: i32,
}

impl Cell {
    fn new(x: i32, y: i32) -> Cell {
        let rack_id = x + 10;
        let mut power_level = rack_id * y;
        power_level += SERIAL_NUM;
        power_level = power_level * rack_id;
        let power_level = power_level.to_string();
        let idx = power_level.len() - 3;
        let power_level: i32 = power_level.get(idx..idx + 1).unwrap_or(String::from("0").as_ref()).parse().unwrap_or(0) - 5;

        Cell { power_level }
    }
}

struct Grid {
    cells: FnvHashMap<(i32, i32), Cell>,
}

impl Grid {
    fn get_most_valuable_square(&self, n: i32) -> ((i32, i32), i32) {
        let mut top_score = std::i32::MIN;
        let mut top_score_coord = (0, 0);
        for x in 1..301 - n {
            for y in 1..301 - n {
                let bounding_box = (x, x + n, y, y + n);
                let mut cells_sum = 0;
                for x_range in bounding_box.0..bounding_box.1 {
                    for y_range in bounding_box.2..bounding_box.3 {
                        match self.cells.get(&(x_range, y_range)) {
                            Some(v) => cells_sum += v.power_level,
                            _ => {}
                        };
                    }
                }

                if cells_sum > top_score {
                    top_score_coord = (x, y);
                    top_score = cells_sum;
                }
            }
        }
        println!("{} for {:?}", top_score, (top_score_coord, n));
        (top_score_coord, top_score)
    }
}

fn main() {
    let mut cells: FnvHashMap<(i32, i32), Cell> = FnvHashMap::with_capacity_and_hasher(300 * 300, Default::default());
    for x in 1..=300 {
        for y in 1..=300 {
            cells.insert((x, y), Cell::new(x, y));
        }
    }
    let grid = Grid { cells };
    // println!("{:?}", grid.get_most_valuable_square(3));
    let scores: Vec<i32> = (1..=300).collect();
    // let highest_score = scores.iter().map(|&i| (grid.get_most_valuable_square(i), i)).fold((((0, 0), 0), 0), |acc, v| if (acc.0).1 < (v.0).1 { v } else { acc });
    let highest_score = scores.par_iter().map(|&i| (grid.get_most_valuable_square(i), i)).reduce(|| (((0, 0), 0), 0), |acc, v| if (acc.0).1 < (v.0).1 { v } else { acc });
    println!("{:?}", highest_score);
}
