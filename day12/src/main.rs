#![feature(dbg_macro)]

use std::fs::File;
use std::io::Read;
use std::str::FromStr;
use std::string::ToString;
use std::collections::{BTreeMap, HashMap};

#[derive(Debug, Clone)]
struct Plant {
    alive: bool
}

impl ToString for Plant {
    fn to_string(&self) -> String {
        match self.alive {
            true => format!("#"),
            false => format!(".")
        }
    }
}

impl FromStr for Plant {
    type Err = String;

    fn from_str(s: &str) -> Result<Plant, String> {
        match s {
            "#" => Ok(Plant { alive: true }),
            "." => Ok(Plant { alive: false }),
            _ => Err(format!("Error while parsing {:?}", s))
        }
    }
}

#[derive(Debug)]
struct Garden {
    plants: BTreeMap<i32, Plant>,
    rules: HashMap<String, Plant>,
}

impl Garden {
    fn _print_state(&self) {
        for plant in self.plants.values() {
            print!("{}", plant.to_string());
        }
        print!("\n");
    }

    fn _plants_to_string(&self) -> String {
        self.plants.values().map(|v| v.to_string()).collect()
    }

    fn indices_to_string(&self) -> String {
        let min = self.plants.keys().min().unwrap().to_string();
        let max = self.plants.keys().max().unwrap().to_string();
        format!("Bounds: {} {}", min, max)
    }

    fn run_rounds(&mut self, n: i64) {
        for i in 0..n {
            self.tick();
            print!("Tick #{} Alive: {}\n", i, self.plants.keys().sum::<i32>());
        }
    }

    fn tick(&mut self) {
        let mut new_state = BTreeMap::new();

        for p_index in *self.plants.keys().min().unwrap() - 2..*self.plants.keys().max().unwrap() + 3 {
            let neighborhood = (p_index - 2..p_index + 3).map(|n| self.plants.get(&n).unwrap_or(&Plant { alive: false }));
            let key: String = neighborhood.map(|x| x.to_string()).collect();
            if let Some(rule) = self.rules.get(&key) {
                if rule.alive {
                    new_state.insert(p_index, Plant { alive: true });
                }
            }
        }

        self.plants = new_state;
    }
}

fn main() {
    let mut input_file = File::open("./input").unwrap();
    let mut input = String::new();
    input_file.read_to_string(&mut input).unwrap();
    let mut garden = parse_input(&input);
    println!("{}", garden.indices_to_string());
    garden.run_rounds(1000);
    let sum_alive: i32 = garden.plants.keys().sum();
    println!("Alive: {}", sum_alive);
}

fn parse_input(input: &str) -> Garden {
    let mut lines = input.lines();
    let initial_plants: BTreeMap<i32, Plant> = lines.next().unwrap()
        .split("")
        .skip(16)
        .filter(|v| v != &"\r\n" && v != &"")
        .enumerate()
        .map(|(i, v)| (i as i32, v.parse().unwrap()))
        .collect::<Vec<(i32, Plant)>>()
        .into_iter()
        .filter(|x| x.1.alive)
        .collect();

    lines.next();

    let mut rules = HashMap::new();

    for line in lines {
        let key: String = line[0..5].into();
        let value: Plant = line[9..10].trim().parse().unwrap();
        rules.insert(key, value);
    }

    Garden {
        plants: initial_plants,
        rules
    }
}
