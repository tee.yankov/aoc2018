use std::fs::File;
use std::io::Read;
use std::collections::HashSet;
use std::iter::FromIterator;
use std::sync::{Mutex,Arc};
use rayon::prelude::*;

fn main() {
    let mut input_file = File::open("./input").expect("Could not open input file");
    let mut input = String::new();
    input_file
        .read_to_string(&mut input)
        .expect("Could not read input file");
    let input = input.trim().into();
    let collapsed_input = collapse_string(&input);
    println!("Units Remaining: {}", collapsed_input.len());
    let optimal_layout = find_optimal_layout(&input);
    println!("Optimal Layout Length {}", optimal_layout.len());
}

fn collapse_string(input: &String) -> String {
    let mut output_length = 0;
    let mut previous_output = String::from(input.as_ref());
    loop {
        let output = parse_input(&previous_output);
        if output_length == output.len() {
            break;
        } else {
            output_length = output.len();
            previous_output = output;
        }
    }
    previous_output
}

fn parse_input(input: &String) -> String {
    let mut chars = input.chars().peekable();
    let mut response: Vec<char> = Vec::new();
    while let Some(a) = chars.next() {
        let b = chars.peek();
        match b {
            Some(b) => {
                if a.to_lowercase().next() == b.to_lowercase().next()
                    && (a.is_uppercase() && !b.is_uppercase()
                        || a.is_lowercase() && !b.is_lowercase())
                {
                    chars.next();
                } else {
                    response.push(a);
                }
            }
            None => response.push(a),
        }
    }
    String::from_utf8(response.par_iter().map(|x| *x as u8).collect()).unwrap()
}

fn find_optimal_layout(input: &String) -> String {
    let uniques: HashSet<char> = HashSet::from_iter(input.chars().map(|x| x.to_lowercase().next().unwrap()));
    let shortest_length = Arc::new(Mutex::new(input.len()));
    let shortest_layout = Arc::new(Mutex::new(String::new()));
    let sl = shortest_layout.clone();
    uniques.par_iter().for_each(|c| {
        let input_omitted_c: String = input.chars().filter(|x| x.to_lowercase().next().unwrap() != *c).collect();
        let output = collapse_string(&input_omitted_c);
        if output.len() < *shortest_length.clone().lock().unwrap() {
            let shortest_layout = shortest_layout.clone();
            let shortest_length = shortest_length.clone();
            let mut shortest_layout = shortest_layout.lock().unwrap();
            let mut shortest_length = shortest_length.lock().unwrap();
            *shortest_length = output.len();
            *shortest_layout = output;
        }
    });
    let shortest_layout = sl.lock().unwrap().clone();
    String::from(shortest_layout)
}
