use std::collections::HashMap;
use std::cmp::{min,max};
use std::fs::File;
use std::io::Read;

#[derive(Debug, Clone)]
struct Rect {
    id: i32,
    collision_points: Vec<CollisionPoint>,
    bounding_box: [i32; 4]
}

#[derive(Debug, Clone, Hash)]
struct CollisionPoint(i32, i32);

impl PartialEq for CollisionPoint {
    fn eq(&self, other: &CollisionPoint) -> bool {
        self.0 == other.0 && self.1 == other.1
    }
}
impl Eq for CollisionPoint {}

impl Rect {
    // Parse and build rectangle from string in the shape of
    // #ID @ x,y: wxh
    fn new(input: String) -> Rect {
        let mut chars = input.chars();
        let mut temp_buffer: Vec<char> = vec![];
        let mut main_buffer: Vec<i32> = vec![];
        loop {
            match chars.next() {
                Some(c) => {
                    if c.is_digit(10) {
                        temp_buffer.push(c);
                    } else {
                        if temp_buffer.len() > 0 {
                            main_buffer.push(
                                (String::from_utf8(
                                    temp_buffer.clone().into_iter().map(|x| x as u8).collect(),
                                )
                                .unwrap())
                                .parse()
                                .expect("Parse Error"),
                            );
                        }
                        temp_buffer.truncate(0);
                    }
                }
                None => {
                    if temp_buffer.len() > 0 {
                        main_buffer.push(
                            (String::from_utf8(
                                temp_buffer.clone().into_iter().map(|x| x as u8).collect(),
                            )
                            .unwrap())
                            .parse()
                            .expect("Parse Error"),
                        );
                    }
                    temp_buffer.truncate(0);
                    break;
                }
            }
        }
        assert_eq!(main_buffer.len(), 5);
        let id = main_buffer[0];
        let x = main_buffer[1];
        let y = main_buffer[2];
        let w = main_buffer[3];
        let h = main_buffer[4];
        let rect = Rect {
            id: id,
            collision_points: iter_points(&[x, x + w, y, y + h]),
            bounding_box: [
                x,
                x + w,
                y,
                y + h
            ]
        };
        // println!("{:?}", &rect);
        rect
    }

    fn intersects_with(&self, other: &Rect) -> bool {
        let x_overlap = max(
            0,
            min(self.bounding_box[1], other.bounding_box[1])
                - max(self.bounding_box[0], other.bounding_box[0]),
        );
        let y_overlap = max(
            0,
            min(self.bounding_box[3], other.bounding_box[3])
                - max(self.bounding_box[2], other.bounding_box[2]),
        );
        x_overlap * y_overlap > 0
    }
}

fn iter_points(coords: &[i32; 4]) -> Vec<CollisionPoint> {
    let mut points: Vec<CollisionPoint> = vec![];
    for x in coords[0]..coords[1] {
        for y in coords[2]..coords[3] {
            points.push(CollisionPoint(x, y))
        }
    }
    points
}

fn main() {
    let mut input_file = File::open("./input").expect("Could not find input file");
    let mut input = String::new();
    input_file
        .read_to_string(&mut input)
        .expect("Could not read input file");
    let rects = build_rectangles(&input);
    let collisions = sum_collisions(&rects);
    println!("Collisions: {:?}", collisions);
    match get_unique(&rects) {
        Some(n) => println!("Unique ID: {}", n),
        None => println!("No Unique IDs found"),
    }
}

// Create all Rect structs
fn build_rectangles(input: &String) -> Vec<Rect> {
    let rects: Vec<Rect> = input
        .trim()
        .split('\n')
        .into_iter()
        .map(|x| Rect::new(String::from(x)))
        .collect();
    rects
}

/**
 * Add all collision points to a HashMap with their (x, y)
 * coordinates as a key and a counter as value.
 * Increments counter upon further occurence.
 */
fn sum_collisions(rects: &Vec<Rect>) -> i32 {
    let mut grid: HashMap<CollisionPoint, i32> = HashMap::new();
    for rect in rects.clone() {
        for point in rect.collision_points {
            if grid.contains_key(&point) {
                if let Some(x) = grid.get_mut(&point) {
                    *x += 1;
                }
            } else {
                grid.insert(point, 1);
            }
        }
    }
    // Count all counters above 1
    grid.values()
        .filter(|x| **x > 1)
        .collect::<Vec<&i32>>()
        .len() as i32
}

/**
 * Check all rectangles against all other rectangles
 * using their `intersects_with` method.
 */
fn get_unique(rects: &Vec<Rect>) -> Option<i32> {
    for rect in rects {
        let mut is_unique = true;
        for r in rects {
            if rect.id != r.id && rect.intersects_with(r) {
                is_unique = false;
                break
            }
        }
        if is_unique {
            return Some(rect.id)
        }
    }
    None
}
