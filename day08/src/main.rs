use std::fs::{File};
use std::io::{Read,Error};

fn main() -> Result<(), Error> {
    let mut input_file = File::open("./input")?;
    let mut input = String::new();
    input_file.read_to_string(&mut input)?;
    let root_node = parse_input(&input);
    // println!("{:?}", root_node);
    println!("Sum Meta: {}", root_node.sum_meta());
    println!("Root Node Value: {}", root_node.get_value());
    Ok(())
}

fn parse_input(input: &str) -> Node {
    let mut nodes = input
        .split_whitespace()
        .map(|x| x.parse::<u32>()
             .expect(format!("Could not parse {} to number", x).as_ref()))
        .into_iter();
    parse_node(&mut nodes)
}

#[derive(Debug)]
struct Node {
    children: Vec<Node>,
    meta: Vec<u32>,
}

impl Node {
    fn sum_meta(&self) -> u32 {
        let children_sum = self.children.iter().fold(0, |acc, x| acc + x.sum_meta());
        let meta_sum = self.meta.iter().fold(0, |acc, x| acc + x);
        children_sum + meta_sum
    }

    fn get_value(&self) -> u32 {
        if self.children.is_empty() {
            self.meta.iter().fold(0, |acc, x| acc + x)
        } else {
            self.meta.iter().fold(0, |acc, x| {
                acc + match self.children.get((*x - 1) as usize) {
                    Some(n) => n.get_value(),
                    None => 0,
                }
            })
        }
    }
}

fn parse_node(mut input: &mut Iterator<Item=u32>) -> Node {
    let mut node = Node {
        children: vec![],
        meta: vec![],
    };
    let n_children = input.next().unwrap();
    let n_meta = input.next().unwrap();
    for _ in 0..n_children {
        node.children.push(parse_node(&mut input));
    }

    for _ in 0..n_meta {
        node.meta.push(input.next().unwrap());
    }

    node
}
