use std::fs::File;
use std::io::*;
use std::iter::*;
use std::collections::HashSet;

fn main() {
    let mut input_file = File::open("./input").expect("Input file not found");
    let mut input = String::new();
    input_file
        .read_to_string(&mut input)
        .expect("Could not read file to string");

    let sum = sum_rows(&input);
    println!("Sum: {}", sum);
    let regular = find_regular(&input);
    println!("Regular: {}", regular);
}

fn parse_input(input: &String) -> Vec<i32> {
    input
        .split_whitespace()
        .map(|x| x.parse().expect(format!("Could not parse {}", x).as_ref()))
        .collect()
}

fn sum_rows(input: &String) -> i32 {
    parse_input(input).into_iter().fold(0, |acc, x| acc + x)
}

fn find_regular(input: &String) -> i32 {
    let nums = parse_input(input);
    let mut nums = nums.iter().cloned().cycle();
    let mut acc = 0;
    let mut hset: HashSet<i32> = HashSet::new();
    let answer: Option<i32>;

    loop {
        let n = nums.next();
        if let Some(n) = n {
            acc += n;
        }

        if hset.contains(&acc) {
            answer = Some(acc);
            break;
        } else {
            hset.insert(acc);
        }
    }

    answer.unwrap()
}
