use indexmap::IndexSet;
use std::cell::RefCell;
use std::collections::BTreeSet;
use std::fs::File;
use std::io::{Error, Read};
use std::rc::Rc;
use std::str::FromStr;
use std::iter::FromIterator;

const CHAR_OFFSET: u32 = 64;
const SECONDS_OFFSET: u32 = 60;
const NUM_WORKERS: u8 = 5;

#[derive(Debug, Hash, PartialEq, Eq)]
struct Step(char, char);

impl FromStr for Step {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Error> {
        let s_chars: Vec<&str> = s.split_whitespace().collect();
        let s_0 = s_chars[1];
        let s_1 = s_chars[7];
        Ok(Step(
            s_0.chars().next().unwrap(),
            s_1.chars().next().unwrap(),
        ))
    }
}

struct Worker {
    id: u8,
    task: Option<u32>,
    value: Option<char>,
}

impl Worker {
    fn assign_task(&mut self, task: u32, c: char) {
        self.task = Some(task);
        self.value = Some(c);
    }
}

struct WorkerPool {
    workers: Vec<Worker>,
    timer: u32,
}

impl WorkerPool {
    fn new(num_workers: u8) -> WorkerPool {
        let mut workers = vec![];
        for i in 0..num_workers {
            workers.push(Worker { id: i, task: None, value: None });
        };
        WorkerPool {
            workers,
            timer: 0
        }
    }

    fn tick(&mut self) {
        self.timer += 1;
    }

    fn available_workers(&self) -> Vec<u8> {
        self.workers.iter().filter(|v| match v.task {
            Some(v) => self.timer >= v,
            None => true,
        }).map(|v| v.id).collect()
    }

    fn get_worker_by_id(&mut self, id: u8) -> Option<&mut Worker> {
        for worker in &mut self.workers {
            if worker.id == id {
                return Some(worker)
            }
        }
        None
    }

    fn run_task(&mut self, t: char) -> bool {
        if let Some(worker_id) = self.available_workers().first() {
            let duration = self.timer + SECONDS_OFFSET + (t as u32 - CHAR_OFFSET);
            let worker = self.get_worker_by_id(*worker_id).unwrap();
            worker.assign_task(duration, t);
            true
        } else {
            false
        }
    }

    fn get_completed_tasks(&self) -> Vec<char> {
        let mut completed_tasks = vec![];
        for worker in self.workers.iter() {
            if let Some(task) = worker.task {
                if task == self.timer {
                    completed_tasks.push(worker.value.unwrap())
                }
            }
        }
        completed_tasks
    }
}

#[derive(Eq, Debug, PartialOrd, Ord)]
struct Node {
    requires: BTreeSet<char>,
    value: char,
    children: BTreeSet<Rc<RefCell<Node>>>,
}

impl PartialEq for Node {
    fn eq(&self, other: &Node) -> bool {
        self.value == other.value
    }
}

impl Node {
    fn new(c: char) -> Node {
        Node {
            value: c,
            children: BTreeSet::new(),
            requires: BTreeSet::new(),
        }
    }

    fn insert_child(&mut self, mut child: Node) {
        if child.value == self.value {
            return;
        }
        child.requires.insert(self.value);
        self.children.insert(Rc::new(RefCell::new(child)));
    }

    fn get_child_by_value(&self, value: char) -> Option<Rc<RefCell<Node>>> {
        for child in self.children.iter() {
            if child.clone().borrow().value == value {
                return Some(child.clone());
            } else if let Some(c) = child.clone().borrow().get_child_by_value(value) {
                return Some(c);
            }
        }

        None
    }

    fn _print_tree(&self, indent_level: usize) {
        let indent = "  ".repeat(indent_level);
        print!("\n{}{}", indent, self.value);
        if !self.children.is_empty() {
            print!(" [");
            for child in &self.children {
                child.clone().borrow()._print_tree(indent_level + 1);
            }
            print!("\n{}]", indent);
        }
    }

    fn _execute(&self) -> String {
        let mut active_nodes: BTreeSet<char> = BTreeSet::new();
        for child in self.children.iter() {
            active_nodes.insert(child.clone().borrow().value);
        }
        let mut execution_order = String::new();

        while let Some(active_value) = active_nodes.clone().into_iter().nth(0) {
            let active_node = self.get_child_by_value(active_value).unwrap().clone();
            let active_node = active_node.borrow();
            active_nodes.remove(&active_value);
            execution_order.push(active_value);
            for child in &active_node.children {
                let child = child.clone();
                let child = child.borrow();
                if child.requires.iter().all(|x| execution_order.contains(*x)) {
                    active_nodes.insert(child.value);
                }
            }
        }
        execution_order
    }

    fn execute_2(&mut self) -> u32 {
        let mut active_nodes: BTreeSet<char> = BTreeSet::new();
        for child in self.children.iter() {
            active_nodes.insert(child.clone().borrow().value);
        }
        let mut worker_pool = WorkerPool::new(NUM_WORKERS);
        let mut execution_order = String::new();
        let mut started_tasks: BTreeSet<char> = BTreeSet::new();

        while !active_nodes.is_empty() {
            // Add completed tasks to execution_order
            let completed_tasks = worker_pool.get_completed_tasks();
            let completed_tasks = BTreeSet::from_iter(completed_tasks.into_iter());
            for task in &completed_tasks {
                execution_order.push(*task);
            }

            for node in active_nodes.clone() {
                let active_node = self.get_child_by_value(node).unwrap().clone();
                let active_node = active_node.borrow();

                // Add new children to active_nodes
                for child in &active_node.children {
                    let child = child.clone();
                    let child = child.borrow();
                    if child.requires.iter().all(|x| execution_order.contains(*x)) {
                        active_nodes.insert(child.value);
                    }
                }
            }

            for task in &completed_tasks {
                active_nodes.remove(task);
            }

            for node in &active_nodes {
                // Add tasks to worker_pool
                if !started_tasks.contains(node) {
                    if worker_pool.run_task(*node) {
                        started_tasks.insert(*node);
                    }
                }
            }


            worker_pool.tick();
        }

        println!("{}", execution_order);
        worker_pool.timer - 1
    }
}

fn main() -> Result<(), Error> {
    let mut input_file = File::open("./input")?;
    let mut input = String::new();
    input_file.read_to_string(&mut input)?;
    let steps = parse_input(&input);
    // Multiple root nodes exist
    let mut steps_clone: BTreeSet<char> = steps.iter().map(|x| x.0).collect();
    for step in &steps {
        steps_clone.remove(&step.1);
    }
    let mut root_node = Node::new('|');
    for step in steps_clone {
        root_node.insert_child(Node::new(step));
    }
    let mut steps_vec = steps.into_iter().collect::<Vec<Step>>();
    while !steps_vec.is_empty() {
        steps_vec.retain(|step: &Step| {
            let existing_child = root_node.get_child_by_value(step.1);
            match root_node.get_child_by_value(step.0) {
                Some(v) => {
                    if let Some(c) = existing_child {
                        c.clone().borrow_mut().requires.insert(step.0);
                        v.clone().borrow_mut().children.insert(c);
                    } else {
                        v.clone().borrow_mut().insert_child(Node::new(step.1));
                    }
                    false
                }
                None => true,
            }
        });
    }
    // println!("{:?}", root_node);
    // root_node._print_tree(0);
    // print!("\n");
    // println!("{}", root_node.execute());
    println!("{}", root_node.execute_2());
    Ok(())
}

fn parse_input(input: &str) -> IndexSet<Step> {
    input.trim().lines().map(|x| x.parse().unwrap()).collect()
}
