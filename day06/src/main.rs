use std::collections::BTreeMap;
use std::fs::File;
use std::io::{Error, Read};
use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    fn distance(&self, other: &Point) -> i32 {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
}

impl FromStr for Point {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let coords: Vec<&str> = s.split(',').collect();

        let x_fromstr = coords[0].parse::<i32>()?;
        let y_fromstr = coords[1].trim().parse::<i32>()?;

        Ok(Point::new(x_fromstr, y_fromstr))
    }
}

#[derive(Debug)]
struct Grid {
    points: Vec<Point>,
    bounding_box: (i32, i32, i32, i32),
}

impl Grid {
    fn new(points: Vec<Point>) -> Grid {
        let bounding_box: (i32, i32, i32, i32) =
            points
                .iter()
                .fold((std::i32::MAX, 0, std::i32::MAX, 0), |mut acc, v| {
                    if v.x < acc.0 {
                        acc.0 = v.x;
                    }
                    if v.x > acc.1 {
                        acc.1 = v.x;
                    }
                    if v.y < acc.2 {
                        acc.2 = v.y;
                    }
                    if v.y > acc.3 {
                        acc.3 = v.y;
                    }
                    acc
                });
        Grid {
            points,
            bounding_box,
        }
    }

    /**
     * Walk through every point inside the bounding box and gather
     * distances to each of the input points. Sum area for individual points
     * and exclude infinite areas.
     */
    fn walk_grid(&mut self) -> i32 {
        // Area of every Point
        let mut area_map: BTreeMap<&Point, i32> = BTreeMap::new();
        // Points having points at the edge (will need to be excluded later)
        let mut infinite_points: Vec<&Point> = Vec::new();
        let mut largest_region = 0;
        // Iterate over X and Y coordinates in bounding box
        for x in self.bounding_box.0..=self.bounding_box.1 {
            for y in self.bounding_box.2..=self.bounding_box.3 {
                let current_point = Point::new(x, y);
                let mut point_distance: Vec<(&Point, i32)> = self
                    .points
                    .iter()
                    .map(|p| (p, current_point.distance(&p)))
                    .collect();

                point_distance.sort_by(|&(_, a), &(_, b)| a.cmp(&b));

                let distance_to_all_locations = point_distance.iter().fold(0, |acc, v| acc + v.1);
                if distance_to_all_locations < 10000 {
                    largest_region += 1;
                }

                if point_distance[0].1 == point_distance[1].1 {
                } else {
                    let is_point_on_edge = x == self.bounding_box.0
                                        || x == self.bounding_box.1
                                        || y == self.bounding_box.2
                                        || y == self.bounding_box.3;
                    if is_point_on_edge {
                        infinite_points.push(&point_distance[0].0);
                    }
                    match area_map.get_mut(&point_distance[0].0) {
                        Some(v) => *v += 1,
                        None => {
                            area_map.insert(&point_distance[0].0, 1);
                        }
                    }
                }
            }
        }

        println!("Largest Region: {}", largest_region);

        // Exclude infinite and get largest area
        area_map
            .iter()
            .filter(|(k, _)| infinite_points.iter().all(|v| v != *k))
            .map(|(_, v)| v)
            .fold(0, |acc, &v| if v > acc { v } else { acc })
    }
}

fn main() -> Result<(), Error> {
    let mut input_file = File::open("./input")?;
    let mut input = String::new();
    input_file.read_to_string(&mut input)?;
    let mut grid = parse_input(&input);
    println!("Largest Area: {}", grid.walk_grid());
    Ok(())
}

fn parse_input(input: &String) -> Grid {
    let points: Vec<Point> = input.lines().map(|x| x.parse().unwrap()).collect();
    let grid = Grid::new(points);
    grid
}
