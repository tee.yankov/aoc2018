use std::fs::File;
use std::io::{Read,Error,};

fn main() -> Result<(), Error> {
    let mut input_file = File::open("./input")?;
    let mut input = String::new();
    input_file.read_to_string(&mut input)?;
    let (num_players, last_marble) = parse_input(&input);
    println!("{}", input);
    println!("Top Score: {}", sol(num_players, last_marble));
    println!("Top Score on drugs: {}", sol(num_players, last_marble * 100));
    Ok(())
}

fn parse_input(input: &str) -> (u32, u32) {
    let mut it = input.split_whitespace();
    (it.nth(0).unwrap().parse().unwrap(), it.nth(5).unwrap().parse().unwrap())
}

fn sol(num_players: u32, last_marble: u32) -> u32 {
    let mut scores: Vec<u32> = (0..num_players).map(|_| 0).collect();
    let mut active_player = (0..num_players).cycle();
    let mut playing_field: Vec<u32> = vec![0];
    let mut active_marble_index: usize = 0;
    active_player.next();

    for i in 1..=last_marble {
        let next_marble = i;
        let active_player_index = active_player.next().unwrap() as usize;

        if next_marble % 23 == 0 {
            let target_marble_index = (0..playing_field.len()).rev().cycle().nth((playing_field.len() - active_marble_index) + 6).unwrap();
            let target_marble = playing_field.get(target_marble_index).unwrap();

            scores[active_player_index] += next_marble;
            scores[active_player_index] += target_marble;
            playing_field.remove(target_marble_index);
            active_marble_index = target_marble_index;
        } else {
            let next_marble_index = (0..playing_field.len()).cycle().nth(active_marble_index + 1).unwrap() + 1;

            playing_field.insert(next_marble_index, next_marble);
            active_marble_index = next_marble_index;
        }
    }

    let highest_score = scores.iter().fold(0, |acc, x| u32::max(acc, *x));
    highest_score
}
