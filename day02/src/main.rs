use std::fs::File;
use std::io::Read;
use std::collections::{BTreeMap, BTreeSet};

fn main() {
    let mut input_file = File::open("./input").expect("No input file found");
    let mut input = String::new();
    input_file.read_to_string(&mut input).expect("Could not read file to String");
    let check = checksum(&input);
    println!("Checksum: {}", check);
    let box_id = find_boxes(&input);
    println!("Box ID: {}", box_id);
}

fn parse_input(input: &String) -> Vec<String> {
    input.split_whitespace().map(|x| String::from(x)).collect()
}

fn checksum(input: &String) -> u32 {
    let ids = parse_input(input);
    let mut has2 = 0;
    let mut has3 = 0;
    for id in ids {
        let mut hm: BTreeMap<char, u32> = BTreeMap::new();
        for c in id.chars() {
            if hm.contains_key(&c) {
                let v = hm.get_mut(&c).unwrap();
                *v += 1;
            } else {
                hm.insert(c, 1);
            }
        }
        let vals = hm.values().collect();
        let set = BTreeSet::from(vals);
        if set.contains(&3) {
            has3 += 1;
        }
        if set.contains(&2) {
            has2 += 1;
        }
    }
    has2 * has3
}

fn find_boxes(input: &String) -> String {
    let ids = parse_input(input);
    let ids_clone = ids.clone();
    let mut answers: Vec<String> = vec!();
    for id in ids {
        let mut ids_clone = ids_clone.clone();
        ids_clone.retain(|x| {
            let mut differences = 0;
            for (i, c) in x.chars().enumerate() {
                match id.chars().nth(i) {
                    Some(n) => {
                        if n != c {
                            differences += 1;
                        }
                    },
                    _ => {}
                };
            }
            differences == 1
        });
        if ids_clone.len() > 0 {
            answers.push(ids_clone[0].clone());
        }
    }

    let mut box_id: Vec<u8> = vec!();
    if answers.len() > 0 {
        for (i, c1) in answers[0].chars().enumerate() {
            match answers[1].chars().nth(i) {
                Some(c2) => {
                    if c1 == c2 {
                        box_id.push(c1 as u8);
                    }
                },
                _ => {}
            }
        }
        String::from_utf8(box_id).unwrap()
    } else {
        format!("")
    }
}
